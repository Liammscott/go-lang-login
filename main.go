package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

//Function makes server mux, associates handler functions to URLs and returns the mux
func newRouter() *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/", LoginPageHandler).Methods("GET")

	r.HandleFunc("/index", IndexPageHandler).Methods("GET")

	r.HandleFunc("/login", LoginHandler).Methods("POST")

	r.HandleFunc("/register", RegisterPageHandler).Methods("GET")
	r.HandleFunc("/register", RegisterHandler).Methods("POST")

	r.HandleFunc("/logout", LogoutHandler).Methods("POST")
	return r
}

//Call the mux, listen and serve the mux on router
func main() {
	psqlInfo := fmt.Sprintf("host=localhost port=5433 user=scottl " +
		"dbname=blog sslmode=disable")

	db, err := sql.Open("postgres", psqlInfo)

	if err != nil {
		panic(err)
	}
	err = db.Ping()

	if err != nil {
		panic(err)
	}

	InitStore(&dbStore{db: db})
	router := newRouter()

	log.Println("Listening...")
	http.ListenAndServe(":8080", router)
}
