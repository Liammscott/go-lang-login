package main

import (
	"database/sql"
	"fmt"
	"testing"

	// The "testify/suite" package is used to make the test suite
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/suite"
)

type StoreSuite struct {
	suite.Suite
	/*
		The suite is defined as a struct, with the store and db as its
		attributes. Any variables that are to be shared between tests in a
		suite should be stored as attributes of the suite instance
	*/
	store *dbStore
	db    *sql.DB
}

func (s *StoreSuite) SetupSuite() {
	/*
		The database connection is opened in the setup, and
		stored as an instance variable,
		as is the higher level `store`, that wraps the `db`
	*/
	psqlInfo := fmt.Sprintf("host=localhost port=5432 user=scottl " +
		"dbname=blog sslmode=disable")
	// connString := "dbname=<blog> sslmode=disable"
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		s.T().Fatal(err)
	}
	s.db = db
	s.store = &dbStore{db: db}
}

func (s *StoreSuite) SetupTest() {
	/*
		We delete all entries from the table before each test runs, to ensure a
		consistent state before our tests run. In more complex applications, this
		is sometimes achieved in the form of migrations
	*/
	_, err := s.db.Query("DELETE FROM blogusers")
	if err != nil {
		s.T().Fatal(err)
	}
}

func (s *StoreSuite) TearDownSuite() {
	// Close the connection after all tests in the suite finish
	s.db.Close()
}

// This is the actual "test" as seen by Go, which runs the tests defined below
func TestStoreSuite(t *testing.T) {
	s := new(StoreSuite)
	suite.Run(t, s)
}

func (s *StoreSuite) TestCreateBird() {
	// Create a bird through the store `CreateBird` method
	s.store.CreateUser(&UserAcc{
		user_name: "test user name",
		password:  "test password",
	})

	// Query the database for the entry we just created
	res, err := s.db.Query(`SELECT COUNT(*) FROM blogusers WHERE password='test password' AND user_name='test user name'`)
	if err != nil {
		s.T().Fatal(err)
	}

	// Get the count result
	var count int
	for res.Next() {
		err := res.Scan(&count)
		if err != nil {
			s.T().Error(err)
		}
	}

	// Assert that there must be one entry with the properties of the bird that we just inserted (since the database was empty before this)
	if count != 1 {
		s.T().Errorf("incorrect count, wanted 1, got %d", count)
	}
}

func (s *StoreSuite) TestGetUser() {
	// Insert a sample bird into the `birds` table
	_, err := s.db.Query(`INSERT INTO blogusers (user_name, password) VALUES('user_name_test','password_test')`)
	if err != nil {
		s.T().Fatal(err)
	}

	// Get the list of birds through the stores `GetBirds` method
	user_accs, err := s.store.GetUsers()
	if err != nil {
		s.T().Fatal(err)
	}

	// Assert that the count of birds received must be 1
	nUserAccs := len(user_accs)
	if nUserAccs != 1 {
		s.T().Errorf("incorrect count, wanted 1, got %d", nUserAccs)
	}

	// Assert that the details of the bird is the same as the one we inserted
	expectedUserAcc := UserAcc{"user_name_test", "password_test"}
	if *user_accs[0] != expectedUserAcc {
		s.T().Errorf("incorrect details, expected %v, got %v", expectedUserAcc, *user_accs[0])
	}
}
