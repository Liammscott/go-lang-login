package main

import (
	"database/sql"
	"log"
)

type Store interface {
	CreateUser(user *UserAcc) error
	GetUsers() ([]*UserAcc, error)
}

type dbStore struct {
	db *sql.DB
}

func (store *dbStore) CreateUser(user *UserAcc) error {
	_, err := store.db.Query("INSERT INTO blogusers(user_name, password) VALUES ($1,$2)", user.user_name, user.password)
	log.Println("\"INSERT INTO blogusers(user_name, password) VALUES (", user.user_name, user.password, ")\"")
	return err
}

func (store *dbStore) GetUsers() ([]*UserAcc, error) {
	// Query the database for all BlogUsers, and return the result to the
	// `rows` object
	rows, err := store.db.Query("SELECT user_name, password from blogusers")
	// We return incase of an error, and defer the closing of the row structure
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	// Create the data structure that is returned from the function.
	// By default, this will be an empty array of birds
	user_accs := []*UserAcc{}
	for rows.Next() {
		// For each row returned by the table, create a pointer to a bird,
		user_acc := &UserAcc{}
		// Populate the `user_name` and `description` attributes of the bird,
		// and return incase of an error
		if err := rows.Scan(&user_acc.user_name, &user_acc.password); err != nil {
			return nil, err
		}
		// Finally, append the result to the returned array, and repeat for
		// the next row

		user_accs = append(user_accs, user_acc)
	}

	return user_accs, nil
}

// The store variable is a package level variable that will be available for
// use throughout our application code
var store Store

/*
We will need to call the InitStore method to initialize the store. This will
typically be done at the beginning of our application (in this case, when the server starts up)
This can also be used to set up the store as a mock, which we will be observing
later on
*/
func InitStore(s Store) {
	store = s
}
