package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/securecookie"
	"golang.org/x/crypto/bcrypt"
)

type UserAcc struct {
	user_name string
	password  []byte
}

//The hashKey is required, used to authenticate the cookie value using HMAC. It is recommended to use a key with 32 or 64 bytes.
//The blockKey is optional, used to encrypt the cookie value -- set it to nil to not use encryption.
// syntax is var sc = securecookie.New(hashKey, blockKey)
var cookieHandler = securecookie.New(
	securecookie.GenerateRandomKey(64),
	securecookie.GenerateRandomKey(32))

// for GET
//Get request loads the login html
func LoginPageHandler(response http.ResponseWriter, request *http.Request) {
	var body, _ = LoadFile("assets/login.html")
	fmt.Fprintf(response, body)
}

// for POST
//Post request sends form, checks if user is valid, returns to index and sets up cookie if so
func LoginHandler(response http.ResponseWriter, request *http.Request) {
	user_accs, err := store.GetUsers()
	user_valid := false

	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		response.WriteHeader(http.StatusInternalServerError)
		return
	}
	name := request.FormValue("name")
	pass := request.FormValue("password")
	redirectTarget := "/"
	hash, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
	if err != nil {
		return
	}
	if !IsEmpty(name) && !IsEmpty(pass) {
		// Database check for user data!
		for _, element := range user_accs {
			log.Println("DB entry: ", element)
			log.Println("Login info: ", name, " ", string(hash))
			if name == element.user_name && bcrypt.CompareHashAndPassword(element.password, []byte(pass)) == nil {
				user_valid = true
			}
		}

		if user_valid {
			SetCookie(name, response)
			redirectTarget = "/index"
		} else {
			redirectTarget = "/register"
		}
	}

	http.Redirect(response, request, redirectTarget, 302)
}

// for GET
//Get request loads register.html
func RegisterPageHandler(response http.ResponseWriter, request *http.Request) {
	var body, _ = LoadFile("assets/register.html")
	fmt.Fprintf(response, body)
}

// for POST
//Post request checks user input and makes a user if valid
// for POST
func RegisterHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	uName := r.FormValue("username")
	pwd := r.FormValue("password")
	confirmPwd := r.FormValue("passwordconf")

	_uName, _pwd, _confirmPwd := false, false, false
	_uName = !IsEmpty(uName)
	_pwd = !IsEmpty(pwd)
	_confirmPwd = !IsEmpty(confirmPwd)

	if _uName && _pwd && _confirmPwd {
		cost := bcrypt.DefaultCost
		hash, err := bcrypt.GenerateFromPassword([]byte(pwd), cost)
		log.Println("byte hash", hash)
		if err != nil {
			return
		}
		fmt.Fprintln(w, "Username for Register : ", uName)
		fmt.Fprintln(w, "Password for Register : ", pwd)
		fmt.Fprintln(w, "ConfirmPassword for Register : ", confirmPwd)
		fmt.Fprintln(w, "Hashed Password for Register : ", hash)
		user_acc := UserAcc{uName, hash}
		store.CreateUser(&user_acc)

	} else {
		fmt.Fprintln(w, "This fields can not be blank!")
	}
}

// for GET
//loads index if get is called
func IndexPageHandler(response http.ResponseWriter, request *http.Request) {
	userName := GetUserName(request)
	if !IsEmpty(userName) {
		var indexBody, _ = LoadFile("assets/index.html")
		fmt.Fprintf(response, indexBody, userName)
	} else {
		http.Redirect(response, request, "/", 302)
	}
}

// for POST
//logs out if post called, clears cookie
func LogoutHandler(response http.ResponseWriter, request *http.Request) {
	ClearCookie(response)
	http.Redirect(response, request, "/", 302)
}

// Cookie

func SetCookie(userName string, response http.ResponseWriter) {
	value := map[string]string{
		"name": userName,
	}

	if encoded, err := cookieHandler.Encode("cookie", value); err == nil {
		cookie := &http.Cookie{
			Name:  "cookie",
			Value: encoded,
			Path:  "/",
		}
		http.SetCookie(response, cookie)
	}
}

func ClearCookie(response http.ResponseWriter) {
	cookie := &http.Cookie{
		Name:   "cookie",
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}
	http.SetCookie(response, cookie)
}

func GetUserName(request *http.Request) (userName string) {
	if cookie, err := request.Cookie("cookie"); err == nil {
		cookieValue := make(map[string]string)
		if err = cookieHandler.Decode("cookie", cookie.Value, &cookieValue); err == nil {
			userName = cookieValue["name"]
		}
	}
	return userName
}
