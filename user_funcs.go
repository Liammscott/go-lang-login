package main

import (
	"errors"
	"io/ioutil"
)

func hasher(pass string) string {
	return pass + "#"
}
func IsEmpty(data string) bool {
	if len(data) == 0 {
		return true
	} else {
		return false
	}
}
func checkRegister(username string, password string, passwordconf string) (bool, error) {

	if IsEmpty(username) || IsEmpty(password) || IsEmpty(passwordconf) {

		return false, errors.New("There is empty data")
	} else if passwordconf == password {
		return true, nil
	} else if passwordconf != password {
		return false, errors.New("password confirm does not match password")
	}
	return true, nil
}

//test DB
func UserIsValid(uName, pwd string) bool {
	// DB simulation
	// _uName, _pwd, _isValid := "liam", "password1", false
	return true
}

func LoadFile(fileName string) (string, error) {
	bytes, err := ioutil.ReadFile(fileName)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}
